package com.example.springbootbtb;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {
    int IncreamID=3;
   static ArrayList<Customer> customers=new ArrayList<>();
    public CustomerController(){
        customers.add(new Customer(1,"Mary","Female",21,"phnom penh"));
        customers.add(new Customer(2,"nara","Female",20,"BTB"));
        customers.add(new Customer(3,"dara","male",19,"KPC"));
    }
    //get all customer
    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getCustomers(){
        return ResponseEntity.ok(new CustomerRespone(
                "This record has found successfully",
                LocalDateTime.now(),
                "OK",
                customers

        ));
    }
    //insert Customer
    @PostMapping("/api/v1/customers")
    public ResponseEntity<CustomerRespone<Customer>> insertCustomer(@RequestBody CustomerInput customerInput){
        IncreamID=IncreamID+1;

        customers.add(new Customer(IncreamID, customerInput.getName(),customerInput.getGender(),customerInput.getAge(),customerInput.getAddress() ));
        Customer customer=new Customer();

        for (Customer cus:customers) {
            if(cus.getId()==IncreamID){
                customer=cus;
                break;
            }
        }
        return ResponseEntity.ok(new CustomerRespone<>(
                "This record was successfully created",
                LocalDateTime.now(),
                "ok",
                customer
        ));


    }
    //getCustomerByid
    @GetMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<CustomerRespone> getCustomerByid(@PathVariable Integer customerId){
        for (Customer cus:customers) {
            if(cus.getId()==customerId){
                return ResponseEntity.ok(new CustomerRespone(
                        "This record has found successfully",
                        LocalDateTime.now(),
                        "ok",
                        new Customer(cus.getId(),cus.getName(),cus.getGender(),cus.getAge(),cus.getAddress())

                ));
            }
        }
        return ResponseEntity.notFound().build() ;
    }
    //searchCustomerByName
    @GetMapping("/api/v1/customers/Search")
    public ResponseEntity<CustomerRespone> searchCustomerByName(@RequestParam String name){
        for (Customer cus:customers) {
            if(cus.getName().equals(name)){
                return ResponseEntity.ok(new CustomerRespone(
                        "This record has found successfully",
                        LocalDateTime.now(),
                        "ok",
                        new Customer(cus.getId(),cus.getName(),cus.getGender(),cus.getAge(),cus.getAddress())
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    // UpdateCustomer
    @PutMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<CustomerRespone> UpdateCustomer(@PathVariable Integer customerId, @RequestBody CustomerInput customerInput){
        for (Customer cus:customers) {
            if(cus.getId()==customerId){
                cus.setName( customerInput.getName());
                cus.setGender(customerInput.getGender());
                cus.setAge(customerInput.getAge());
                cus.setAddress(customerInput.getAddress());

                 return ResponseEntity.ok(new CustomerRespone(
                        "You're update successfully",
                        LocalDateTime.now(),
                        "ok",
                        new Customer(cus.getId(),cus.getName(),cus.getGender(),cus.getAge(),cus.getAddress())
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    //DeleteCustomer
    @DeleteMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<CustomerRespone> DeleteCustomer(@PathVariable Integer customerId ,@RequestBody CustomerInput customerInput){
        for (Customer cus:customers) {
            if(cus.getId()==customerId){
                customers.remove(cus);
                return ResponseEntity.ok(new CustomerRespone(
                        "Congratulation your delete is successfully",
                        LocalDateTime.now(),
                        "ok",
                       null
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }



}
