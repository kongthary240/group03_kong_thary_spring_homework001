package com.example.springbootbtb;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

public class CustomerRespone<T> {
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String status;
    private LocalDateTime time;
    public void setMessage(String message) {
        this.message = message;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }


    public String getMessage() {
        return message;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public T getPayload() {
        return payload;
    }



    public CustomerRespone(String message, LocalDateTime time, String status, T payload) {
        this.message = message;
        this.time = time;
        this.status = status;
        this.payload = payload;
    }


}
