package com.example.springbootbtb;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@OpenAPIDefinition(
		info = @ Info(title = "Customer REST API")
)
@RestController
public class SpringBootBtbApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBootBtbApplication.class, args);

	}


}


